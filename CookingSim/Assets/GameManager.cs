using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.AffordanceSystem.Receiver.Primitives;

public class GameManager : MonoBehaviour
{
    public int puntos;
    public float temps;
    public float time;
    public bool onpartida;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("timer");
        this.onpartida = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator timer()
    {
        while (time<temps)
        {
            time += 1;
            yield return new WaitForSeconds(1);
            this.onpartida = false;
        }
    }  
}
