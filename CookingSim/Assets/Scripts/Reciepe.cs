using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Reciepe : MonoBehaviour
{
    public GameManager GameManager;
    public List<Food> foodlist= new List<Food>();

    public ChooseReciepe c;
    public int index=0;
    public ParticleSystem ps;

    public AudioClip auerror;
    public AudioClip ausuccess;

    public AudioSource aus;
    public GameEvent changereciepe;
    public GameEvent CreateMeal;

    public InnerComponents ic;
    // Start is called before the first frame update
    void Start()
    {
     
        this.aus=this.gameObject.GetComponent<AudioSource>();

        this.ic=this.transform.GetChild(1).transform.gameObject.GetComponent<InnerComponents>();

    }
    public void getList()
    {
        foodlist.Clear();
        //print("Cojo la nueva comanda");

        int i = 0;
        
        foreach (Transform t in c.ShuffledIngredients)
        {

            foodlist.Add(t.gameObject.GetComponent<Food>());
            i++;
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        if (index==foodlist.Count)
        {
            index = 0;
            if (GameManager.onpartida)
            {

                GameManager.puntos++;
            }
        
            CreateMeal.Raise();
            changereciepe.Raise();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag=="Food")
        {
            Food f = collision.gameObject.GetComponent<Food>();
            if (foodlist[index].foodtype == f.foodtype )
            {
                
                ic.components.Add(collision.transform);
                //Particle color and play
                ps.startColor = Color.green;
                ps.Play();
                //Set sound and play
                aus.clip = this.ausuccess;
                aus.Play();
                //next ingredient required
                index++;

                //f.check();
                print(f.foodtype+" ha entrado");
                ic.reload();
            }
            else
            {
                index = 0;
                boom();
            }


            f.create.Raise();
            Destroy(collision.gameObject);
        }
    }
    public void boom()
    {
        //Particle color and play
        ps.startColor= Color.red;
        ps.Play();
        //Set sound and play
        aus.clip = this.auerror;
        aus.Play();

        changereciepe.Raise();
    }
}
