using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateFood : MonoBehaviour
{
    public Food food;
    public GameObject newfood;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
         
    }
    public void spawn()
    {
        
        GameObject g=Instantiate(food.gameObject);
        g.transform.position = this.transform.position;
        newfood = g;
    }
    public void DestroyFood()
    {
        Destroy(this.newfood);
    }
}
