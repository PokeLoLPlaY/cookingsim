using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class ChooseReciepe : MonoBehaviour
{
    public GameEvent listdone;
    public List<Transform> Ingredients;

    public List<Transform> Ingredientlistwall = new List<Transform>();
    public List<Transform> ShuffledIngredients = new List<Transform>();

    public List<GameObject> gl;

    // Start is called before the first frame update
    void Start()
    {
        changeIngredients();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void changeIngredients()
    {
        if (gl.Count!=0)
        {
            print(gl.Count);
            for (int j = gl.Count-1; j >= 0; j--)
            {

                //print("Entro");
                Destroy(gl[j].gameObject);
                gl.RemoveAt(j);
            }
        }

        List<Transform> IngredientstoShuffle = fillIngredientstoshuffle();
        shuffleIngredients(IngredientstoShuffle);
        int i = 0;
        foreach (Transform ingredient in Ingredientlistwall)
        {
            if (i < ShuffledIngredients.Count)
            {

                GameObject g = Instantiate(ShuffledIngredients[i].gameObject);
                g.gameObject.GetComponent<Rigidbody>().useGravity = false;
                g.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                g.transform.parent = ingredient.transform;
                g.transform.position = ingredient.position;
                g.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                i++;
                gl.Add(g);
            }
            
        }
        if (listdone != null)
        {
            //print("Hola???");
            listdone.Raise();
        }

    } 
    private void shuffleIngredients(List<Transform> IngredientstoShuffle)
    {
        ShuffledIngredients.Clear();
        int eliminados = 0;
        //print("Cambio");
        for (int i = 0; i < Ingredients.Count; i++)
        {
            int n = UnityEngine.Random.Range(0, IngredientstoShuffle.Count);
            if ((int)UnityEngine.Random.Range(0,4)>2 && eliminados<2)
            {

                eliminados++;
                //print("Eliminados " + eliminados);

            }
            else
            {

                ShuffledIngredients.Add(IngredientstoShuffle[n]);
            }
            IngredientstoShuffle.Remove(IngredientstoShuffle[n]);
        }
    }


    public List<Transform> fillIngredientstoshuffle()
    {
        List<Transform> list = new List<Transform>();
        foreach (Transform t in Ingredients)
        {
            list.Add(t);
        }

        return list;
    }


    public void win()
    {

    }
}
