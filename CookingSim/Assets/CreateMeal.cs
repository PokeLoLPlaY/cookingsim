using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateMeal : MonoBehaviour
{
    public Transform meal;
    public Reciepe reciepe;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void spawn()
    {

        GameObject g = Instantiate(meal.gameObject);

        checkIngredients(g);

        
    }

    public void checkIngredients(GameObject g)
    {
        bool tomatoes = false;
        bool garlics = false;
        bool onions = false;
        bool chicken = false;

        foreach(Food f in reciepe.foodlist)
        {
            print(f.foodtype);
            if (f.foodtype==foodtype.pollastre)
            {
                chicken = true;
            }
            else if (f.foodtype == foodtype.ceba)
            {
                onions = true;
            }
            else if (f.foodtype == foodtype.all)
            {
                garlics = true;
            }
            else if (f.foodtype == foodtype.tomaquet)
            {
                tomatoes = true;
            }
        }
        print("Ajo " + garlics + " pollo " + chicken + " cebolla " + onions + " tomate " + tomatoes);
        Amanida a = g.transform.GetComponent<Amanida>();

        if (tomatoes==false)
        {
            print("Entro para desactivar tomate");
            a.Tomatoes.gameObject.SetActive(false);
        }
        if (onions == false)
        {
            print("Entro para desactivar cebolla");
            a.Onions.gameObject.SetActive(false);
        }
        if (chicken == false)
        {
            print("Entro para desactivar pollo");
            a.Chicken.gameObject.SetActive(false);
        }
        if (garlics == false)
        {
            print("Entro para desactivar ajos");
            a.Garlics.gameObject.SetActive(false);
        }

    }
}
