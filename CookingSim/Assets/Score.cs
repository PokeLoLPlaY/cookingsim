using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{

    public GameManager GameManager;

    public TextMeshPro tx;
    // Start is called before the first frame update
    void Start()
    {
        this.tx = this.gameObject.GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        this.tx.text = "SCORE: " +GameManager.puntos;
    }
}
