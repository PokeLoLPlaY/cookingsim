using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public GameManager GameManager;

    public TextMeshPro tx;
    // Start is called before the first frame update
    void Start()
    {
        this.tx = this.gameObject.GetComponent<TextMeshPro>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Round(GameManager.time % 60)<10)
        {
            this.tx.text = "TIME: " + Mathf.Floor(GameManager.time / 60) + ":0" + Mathf.Round(GameManager.time % 60);
        }
        else
        {
            this.tx.text = "TIME: " + Mathf.Floor(GameManager.time / 60) + ":" + Mathf.Round(GameManager.time % 60);
        }
    }
}
