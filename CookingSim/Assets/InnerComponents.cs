using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InnerComponents : MonoBehaviour
{
    public List<Transform> components = new List<Transform>();
    public List<Vector3> Posiciones = new List<Vector3>();
    public List<Vector3> rotaciones = new List<Vector3>();
    public List<GameObject> objspawneds = new List<GameObject>();

    public bool canreload;
    // Start is called before the first frame update
    void Start()
    {
        canreload = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void reload()
    {
        //print("Intento reload");
        if (canreload)
        {
            //print("reload");
            for (int i = 0; i < components.Count; i++)
            {
                if (components[i] != null)
                {

                    print(components[i].transform.GetComponent<Food>().foodtype);
                    canreload = false;
                    GameObject g = Instantiate(components[i].gameObject);

                    g.transform.tag = "Untagged";
                    //print(g.transform.tag);

                    g.layer = 3;
                    objspawneds.Add(g);
                    g.gameObject.GetComponent<Rigidbody>().useGravity = false;
                    g.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    g.transform.position = Posiciones[i];
                    g.transform.eulerAngles = rotaciones[i];
                    g.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
                }
                else
                {
                    print("Algo fue ma");
                }
                StartCoroutine("canreloadset");
            }
        }
    }
    public void destroyeverything()
    {
        
        for (int i= objspawneds.Count-1;i>=0;i--)
        {
            
            Destroy(objspawneds[i]);
            objspawneds.RemoveAt(i);
            components.RemoveAt(i);
        }
        components.Clear();
        objspawneds.Clear();
    }
    IEnumerator canreloadset()
    {
        yield return new WaitForSeconds(0.25f);
        canreload = true;
    }
}
