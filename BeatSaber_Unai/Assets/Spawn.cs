using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.AffordanceSystem.Receiver.Primitives;

public class Spawn : MonoBehaviour
{
    public CreatePool pool;

    public float cooldown;
    public int cooldownmax;

    //IzqLimits
    public float izq;

    //DerLimits
    public float der;

    //HeighLimits
    public float heighmin;
    public float heighmax;

    public float spawnz;

    public int speed; 
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
        if (cooldown < 0)
        {
            side side = setside();

            GameObject GO = pool.getGO();

            cooldown = cooldownmax;
            GO.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            GO.GetComponent<Rigidbody>().velocity = Vector3.zero;
            
            GO.transform.rotation = Quaternion.identity;
            setType(GO);
            if (side==side.izq)
            {

                
                GO.transform.position = new Vector3(izq, UnityEngine.Random.Range(heighmin, heighmax),spawnz);
                GO.SetActive(true);
                
            }
            if (side == side.der)
            {


                GO.transform.position = new Vector3(der, UnityEngine.Random.Range(heighmin, heighmax), spawnz);
                GO.SetActive(true);

            }
            GO.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, -speed);
        }
        else
        {
            cooldown-=Time.deltaTime;
        }

    }
    public void setType(GameObject Go)
    {
       MeshDestroy md= Go.GetComponent<MeshDestroy>();

        int rn = UnityEngine.Random.Range(0, 4);

        switch (rn)
        {
            case 0:
                md.type = MeshDestroy.PosType.Vertical_Arriba;
                break;
            case 1:
                md.type = MeshDestroy.PosType.Horizontal_Der;
                Go.transform.Rotate(new Vector3(0, 0, -90));
                break;
            case 2:
                md.type = MeshDestroy.PosType.Vertical_Abajo;
                Go.transform.Rotate(new Vector3(0, 0, -180));
                break;
            case 3:
                md.type = MeshDestroy.PosType.Horizontal_Izq;
                Go.transform.Rotate(new Vector3(0, 0, -270));
                break;
        }
    }
    

    public side setside()
    {
        if (UnityEngine.Random.Range(0, 2) > 0) return side.izq;

        return side.der;
    }

    public enum side
    { 
        der, izq
    }


}

