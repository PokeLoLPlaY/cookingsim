using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.VirtualTexturing.Debugging;

public class father : MonoBehaviour
{
    public lightzone lz;
    // Start is called before the first frame update
    void Start()
    {
        lz=this.transform.GetChild(0).GetComponent<lightzone>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Box")
        {
            MeshDestroy box = other.GetComponent<MeshDestroy>();

         
            if (box.lightcolor == lz.lightcolor)
            {
                box.destroyMesh(other.ClosestPoint(transform.position), true);
            }
            else
            {
                box.destroyMesh(other.ClosestPoint(transform.position), false);

            }

        }
    }
}
