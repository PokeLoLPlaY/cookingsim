using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diana : MonoBehaviour
{
    public int points;
    public gameManager gameManager;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void addpoints()
    {
        //gameManager.addPoints(points);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Flecha")
        {
            if (collision.gameObject.GetComponent<arrow>().points)
            {
                collision.gameObject.GetComponent<arrow>().points = false;
                print(this.gameObject.name);
                addpoints();
                collision.transform.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                collision.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
                collision.transform.gameObject.GetComponent<arrow>().desappear();
            }
        }
    }
}
