using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePool : MonoBehaviour
{
    public GameObject GO;
    public int NumOfGOs;

    public GameObject[] listGO;
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] newlistGO= new GameObject[NumOfGOs];

        for(int i=0; i<NumOfGOs;i++)
        {
            GameObject newGO = Instantiate(GO);
            newGO.transform.gameObject.SetActive(false);
            newGO.transform.parent = transform;
            newlistGO[i]= newGO;
        }
        listGO = newlistGO;
    }

    public GameObject getGO()
    {
        foreach(GameObject go in listGO)
        {

            if (!go.activeSelf)
            {
                return go;
            }

        }
        return null;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
