using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    public int Totalpoints;
    public bool canGetPoints;
    // Start is called before the first frame update
    void Start()
    {
        canGetPoints = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addPoints(int points)
    {
        if (canGetPoints)
        {

            this.Totalpoints += points;
            this.canGetPoints = false;
            StartCoroutine("GetPointsCooldown");

        }
    }
    public IEnumerator GetPointsCooldown()
    {
        yield return new WaitForSeconds(0.5f);
        this.canGetPoints = true;
    }

}
