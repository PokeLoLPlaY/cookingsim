using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arrow : MonoBehaviour
{
    public bool points;
    Rigidbody rb;
    public bool canspin;
    // Start is called before the first frame update
    void Start()
    {
        rb= GetComponent<Rigidbody>();
        canspin = false;
        points = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(canspin)
        SpinObjectInAir();
    }
    public void desappear()
    {
        StartCoroutine("mevoy"); 
    }
    IEnumerator mevoy()
    {
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
    }
    void SpinObjectInAir()
    {
        float yvelocity= rb.velocity.y;
        float zvelocity= rb.velocity.z;
        float xvelocity= rb.velocity.x;
        float combinedVelocity = Mathf.Sqrt(xvelocity * xvelocity + zvelocity * zvelocity);
        float fallAngle = Mathf.Atan2(yvelocity, combinedVelocity) * 180 / Mathf.PI;

        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -fallAngle);
    }
    private void OnCollisionEnter(Collision collision)
    {
        this.canspin = false;
    }
}
