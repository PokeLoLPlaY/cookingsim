using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arcoController : MonoBehaviour
{
    public GameObject arrowPrefab;
    public GameObject stringGrabObject;
    public GameObject myArrow;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void fireArrow()
    {
        if (myArrow != null)
        {
            myArrow.GetComponent<arrow>().canspin = true;
            //Debug.Log("fire arrow");
            myArrow.transform.parent = null;
            myArrow.GetComponent<Rigidbody>().useGravity = true;
            myArrow.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(-10,0,0), ForceMode.Impulse);
            //StartCoroutine(triggerDelay());
            //myArrow.GetComponent<arrowController>().enabled = true;
            //myArrow.GetComponent<arrowController>().enabled = true;
        }

    }

    public void spawnArrow()
    {
        GameObject newArrow = Instantiate(arrowPrefab);
        myArrow = newArrow;

        myArrow.GetComponent<Rigidbody>().useGravity = false;
        //myArrow.GetComponent<arrowController>().enabled = true;
        myArrow.transform.parent = stringGrabObject.transform;
        myArrow.transform.position = stringGrabObject.transform.position;
        myArrow.transform.rotation = stringGrabObject.transform.rotation;
        //myArrow.GetComponent<Rigidbody>().useGravity = false;

    }

    public IEnumerator triggerDelay()
    {
        GameObject thisArrow = myArrow;
        myArrow = null;
        yield return new WaitForSeconds(0.04f);
        thisArrow.GetComponent<CapsuleCollider>().isTrigger = false;
    }


}
