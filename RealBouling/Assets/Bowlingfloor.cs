using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bowlingfloor : MonoBehaviour
{
    public float force;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag=="Ball")
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(-force,0,0));
        }
    }
}
