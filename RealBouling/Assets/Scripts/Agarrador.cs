using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using UnityEngine;
using static recogedor;

public class Agarrador : MonoBehaviour
{

    public List<GameObject> Pins = new List<GameObject>();
    public List<GameObject> invisPins = new List<GameObject>();

    public Vector3 initialPos;

    public float Yspd;

    public float YTime;
    public float CogerTime;

    public float currenttime;

    public agarradorstates state;

    public Rigidbody rb;

    public bool haspins;

    public GameEvent callrecogedor;

    public GameManager gamemanager;

    public bool newpins;
    // Start is called before the first frame update
    void Start()
    {

        this.initialPos = this.transform.position;

        this.rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        switch (this.state)
        {

            case agarradorstates.None:
                break;
            case agarradorstates.bajar:
                if (this.gamemanager.newround)
                {
                    print("Hago visible los pins");
                    this.gamemanager.newround = false;
                    this.gamemanager.newScore = 0;
                    makevisiblepins();
                    newpins = true;
                }
                if (this.currenttime<YTime)
                {
                    //this.rb.velocity = new Vector3(0,-this.Yspd,0);
                    this.transform.position = new Vector3(this.transform.position.x,this.transform.position.y-this.Yspd*Time.deltaTime,this.transform.position.z);
                    this.currenttime += Time.deltaTime;
                }
                else{
                    this.currenttime = 0;
                    this.state = agarradorstates.agarrar;
                }
                break;
            case agarradorstates.agarrar:
                if (this.currenttime<this.CogerTime)
                {
                    this.rb.velocity = Vector3.zero;
                    this.currenttime += Time.deltaTime;
                }
                else{
                    if (newpins)
                    {
                        haspins = false;
                        releasePinsForNewRound();
                    }
                    else if (!haspins)
                    {
                        haspins = true;
                        setParent();
                    }
                    else
                    {
                        haspins = false;
                        releasePins();
                    }
                    
                    this.currenttime = 0;
                    this.state = agarradorstates.subir;
                }
                break;
            case agarradorstates.subir:
                if (this.currenttime < YTime)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + this.Yspd * Time.deltaTime, this.transform.position.z);
                    
                    this.currenttime += Time.deltaTime;
                }
                else
                {
                    if (haspins)
                    {
                        print("Tengo pins");
                        this.callrecogedor.Raise();
                    }
                    if (newpins)
                    {
                        newpins = false;
                    }
                    this.currenttime = 0;
                    this.state = agarradorstates.None;
                }
                break;

        }

    }
    public void setParent()
    {

        foreach (GameObject pin in this.Pins) 
        {
            pin.transform.parent = this.transform;
            pin.GetComponent<CapsuleCollider>().isTrigger = true;
            pin.GetComponent<Rigidbody>().useGravity = false;
            pin.transform.rotation = Quaternion.Euler(0,0,0);
            pin.transform.position = pin.GetComponent<Pin>().initialPos;
        }

    }
    public void releasePins()
    {
        foreach(GameObject pin in this.Pins)
        {
            pin.transform.parent = null;
            pin.GetComponent<CapsuleCollider>().isTrigger = false;

            pin.GetComponent<Rigidbody>().useGravity = true;

        }

        this.Pins.Clear();
    }
    public void releasePinsForNewRound()
    {
        foreach (GameObject pin in this.invisPins)
        {
            pin.transform.parent = null;
            pin.GetComponent<CapsuleCollider>().isTrigger = false;
            pin.GetComponent<Pin>().f = false;
            pin.GetComponent<Rigidbody>().useGravity = true;

        }

        this.invisPins.Clear();
    }
    public void makevisiblepins()
    {
        foreach(GameObject pin in this.invisPins)
        {
            pin.SetActive(true);
            pin.GetComponent<Pin>().sound = false;
        }
    }
    public enum agarradorstates
    {
        None,bajar, agarrar, subir
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag=="Pin")
        {
            if(!other.gameObject.GetComponent<Pin>().f)
            {
                this.Pins.Add(other.gameObject);
            }
            
        }
    }
    public void Move()
    {
        print("Me muevo");
        this.state = agarradorstates.bajar;
    }
}
