using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Pin : MonoBehaviour
{
    public Vector3 initialPos;
    public float dessapearTime;
    public bool f;
    public int spd;
    public bool beingreturn;
    public bool sound;
    public AudioSource AS;

    public AudioClip ac;

    // Start is called before the first frame update
    void Start()
    {
        f = false;
        initialPos = transform.position;
        this.AS = GetComponent<AudioSource>();
        sound = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (beingreturn)
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(this.spd, 0, 0);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag=="Ball" || collision.transform.tag=="Pin" && !sound)
        {
            this.AS.clip = ac;
            sound = true;
            this.AS.Play();
        }
    }
    public void setActiveself() 
    {
        this.transform.gameObject.SetActive(true);
    }
    public void dessapear(Transform t)
    {
        this.gameObject.GetComponent<Rigidbody>().useGravity = false;
        this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        this.gameObject.GetComponent <Rigidbody>().angularVelocity = Vector3.zero;
        this.gameObject.GetComponent<CapsuleCollider>().isTrigger = true;
        this.transform.position = new Vector3(initialPos.x, 0, initialPos.z);
        this.transform.rotation = Quaternion.Euler(0, 0, 0);
        this.transform.parent = t;
        this.f = false;
        f = true;
        this.transform.position = new Vector3(this.transform.position.x, 5.6f, this.transform.position.z);
        t.gameObject.GetComponent<Agarrador>().invisPins.Add(this.gameObject);
        this.transform.gameObject.SetActive(false);
        
        
    }
    
}
