using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int round;
    public bool newround;

    public int newScore;
    public List<int> score1;
    public List<int> score2;
    public List<TextMeshPro> TScore1;
    public List<TextMeshPro> TScore2;
    public TextMeshPro tot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public int gettotal() 
    {
        int tot=0;
        for (int i = 0; i <= score1.Count - 1; i++)
        {
            tot += score1[i];

        }
        for (int i = 0; i <= score2.Count - 1; i++)
        {
            tot += score2[i];

        }

        return tot;
    }

    public void restartScene(){

        SceneManager.LoadScene("SampleScene");

    }
}
