using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class callRecogedor : MonoBehaviour
{
    public GameEvent startAgarrador;
    public GameEvent startRecogedor;
    public bool canraise;

    public float calltime;

    public lanzamiento lanza = lanzamiento.first;

    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Ball")
        {
            if (canraise)
            {
                if (this.lanza==lanzamiento.first)
                {
                    print("Entro bola");
                    StartCoroutine("RaiseFirstEvent");
                    this.lanza = lanzamiento.second;

                }
                else if (this.lanza == lanzamiento.second)
                {
                    StartCoroutine("RaiseSecondEvent");
                    gameManager.newround = true;
                    gameManager.round++;
                    lanza = lanzamiento.first;
                }
            }

        }
    }
    public enum lanzamiento
    {
        first,second
    }
    public IEnumerator RaiseFirstEvent()
    {
        canraise = false;
        
        yield return new WaitForSeconds(this.calltime);
        this.startAgarrador.Raise();
        canraise = true;
    }
    public IEnumerator RaiseSecondEvent()
    {
        canraise = false;
        yield return new WaitForSeconds(this.calltime);
        this.startRecogedor.Raise();
        canraise = true;
    }
}
