using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalReturnBall : MonoBehaviour
{
    public int spd;
    public Transform agarrador;
    public GameManager GM;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag=="Ball")
        {
            collision.gameObject.GetComponent<ball>().beingreturn = false;
            collision.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, this.spd);
        }
        else if (collision.transform.tag == "Pin")
        {
            collision.transform.gameObject.GetComponent<Pin>().dessapear(agarrador);
            GM.newScore++;
        }
    }
}
