using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Outreturn : MonoBehaviour
{
    public int spd;
    public bool returning;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Ball")
        {
            if (returning)
            {

                collision.gameObject.GetComponent<ball>().beingreturn = returning;

                collision.gameObject.GetComponent<ball>().spd=spd;
            }
            collision.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(this.spd, collision.gameObject.GetComponent<Rigidbody>().velocity.y, 0);
        }
        else if (collision.transform.tag == "Pin")
        {
            if (!returning)
            {

                collision.gameObject.GetComponent<Pin>().beingreturn = !returning;

                collision.gameObject.GetComponent<Pin>().spd = spd;
            }
            collision.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(this.spd, 0, 0);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Pin")
        {
            if (!returning)
            {

                collision.gameObject.GetComponent<Pin>().beingreturn = returning;

                collision.gameObject.GetComponent<Pin>().spd = spd;
            }
            collision.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
    }
}
