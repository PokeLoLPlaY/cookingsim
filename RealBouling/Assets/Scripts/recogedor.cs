using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class recogedor : MonoBehaviour
{
    public Vector3 initialPos;

    public int Xspd;
    public int Yspd;

    public float YTime;
    public float XTime;

    public float currenttime;

    public recogedorstates state;

    public Rigidbody rb;

    public GameEvent callAgarrador;
    public GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        this.initialPos = this.transform.position;
        this.rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        switch (state)
        {
            case recogedorstates.None:
                break;
            case recogedorstates.bajar:
                if (currenttime < XTime)
                {
                    this.rb.velocity = new Vector3(0, -this.Yspd, 0);
                    currenttime += Time.deltaTime;
                }
                else
                {
                    currenttime = 0;
                    this.state = recogedorstates.recoger;
                    if(this.gameManager.newround)
                    {
                        this.gameManager.TScore2[gameManager.score2.Count].text = ""+gameManager.newScore;
                        this.gameManager.score2.Add(gameManager.newScore);

                        this.gameManager.tot.text = "" + this.gameManager.gettotal();
                        this.gameManager.newScore = 0;
                    }
                }
                break;
            case recogedorstates.recoger:
                if (currenttime < XTime)
                {
                    this.rb.velocity = new Vector3(-this.Xspd, 0, 0);
                    currenttime += Time.deltaTime;
                }
                else
                {
                    currenttime = 0;
                    this.state = recogedorstates.subir;
                }
                break;
            case recogedorstates.subir:
                if (currenttime < XTime)
                {
                    this.rb.velocity = new Vector3(0, this.Yspd, 0);
                    currenttime += Time.deltaTime;
                }
                else
                {
                    currenttime = 0;
                    this.state = recogedorstates.volver;
                }
                break;
            case recogedorstates.volver:
                if (currenttime < XTime)
                {
                    this.rb.velocity = new Vector3(this.Xspd, 0, 0);
                    currenttime += Time.deltaTime;
                }
                else
                {
                    if (!this.gameManager.newround)
                    {

                        this.gameManager.TScore1[gameManager.score1.Count].text = "" + gameManager.newScore;
                        this.gameManager.score1.Add(gameManager.newScore);
                        this.gameManager.tot.text = "" + this.gameManager.gettotal();
                        this.gameManager.newScore = 0;
                    }
                    

                    this.rb.velocity= Vector3.zero;
                    currenttime = 0;
                    this.state = recogedorstates.None;
                    this.transform.position = this.initialPos;
                    this.callAgarrador.Raise();
                }
                break;
        }

    }

    public void Move()
    {
        this.state = recogedorstates.bajar;
    }
    public enum recogedorstates
    {
        None, bajar, recoger, subir, volver 
    }
}
