# CookingSim 

[+ Risketos minims: +]

    - El jugador es pot moure per l'escena.
    - El jugador pot agafar els ingredients
    - Hi ha efectes de so, un quan el jugador acaba correctament la repceta i un altre quan s'equivoca.

[- Risketos addicionals: -]

    - Hi ha un timer que, quan arriba a 2 minuts, ja no permet al jugador guanyar més punts.
    - Un cop acabat la recepta sense errors apareix un plat personalitzat amb els ingredients que el jugador ha posat.
    - Quan afageixes un ingredient, apareix dins del bol. 
    - Tot funciona mitjançant GameEvents.
    - La repceta és aleatoria i pot tenir 2 3 o 4 ingredients. 
    - La recepta és visible pel jugador.
    - Si el jugador s'equivoca, la repceta es recarrega i el jugador a de començar des del principi.
    - Hi ha un efecte de particules que es cridan un cop el jugador acaba el plat(Les particules seran de color verd) o quan el jugador s'equivoqui(Les 
      perticules seran de color vermell).

# BowlingSim
    
[+ Risketos minims: +]

    - Han d’aparéixes els elements indicats anteriorment.
    - El jugador ha de poder moure’s per l’escena via teleport o locomoció normal amb joystick. 
    - Les bitlles han de tenir la física adequada.
    - Utilitzar l’auto hand perquè el control sigui el més aproximat a la realitat.
    - Palanca de reinici de l’escena. 

[- Risketos addicionals: -]

    - Afegir so en general.
    - Crear un marcador basat en el joc real amb puntuació.

# BowSim

[+ Risketos minims: +]

    - Hi ha una Diana
    - Hi ha un Arc funcional
    - Hi ha una fletxa funcional
    - Hi ha una puntuació
